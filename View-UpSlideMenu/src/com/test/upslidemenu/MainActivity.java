package com.test.upslidemenu;



import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.test.upslidemenu.UpSlideMenuView.ActionItem;
import com.test.upslidemenu.UpSlideMenuView.OnActionClickListener;

public class MainActivity extends Activity {

	boolean isShowing;
	Animation dismissAnim ,showAnim;
	 
	UpSlideMenuView bottomLayout;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		showAnim = AnimationUtils.loadAnimation(this,
				R.anim.bottom_anim_show);
		dismissAnim = AnimationUtils.loadAnimation(this,
				R.anim.bottom_anim_dismiss);
		
		
		bottomLayout = (UpSlideMenuView)findViewById(R.id.upsetting);
		ActionItem ai = new ActionItem("test", 0, 0);
//		bottomLayout.addAction(new ActionItem("test", 0, 1));
		bottomLayout.setOnClickActionListener(new OnActionClickListener() {
			public void OnClickAction(int actionId) {
				
			}}
		);
		
		bottomLayout.addAction(ai);
		
		Button btn = (Button) findViewById(R.id.button1);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isShowing)
				{
					isShowing = false;
					bottomLayout.setVisibility(View.GONE);
					bottomLayout.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,
							R.anim.bottom_anim_dismiss));
					
				}else
				{
					isShowing = true;
					bottomLayout.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,
							R.anim.bottom_anim_show));	
					bottomLayout.setVisibility(View.VISIBLE);
				}
				
			}
		});
		
	}

	

}
