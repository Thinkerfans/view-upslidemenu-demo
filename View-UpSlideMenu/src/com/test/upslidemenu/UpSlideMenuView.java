package com.test.upslidemenu;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UpSlideMenuView extends LinearLayout {

	private List<ActionItem> mActions;
	private LayoutInflater mInflater;
	
	private OnActionClickListener mClickListener;
	
	public void setOnClickActionListener(OnActionClickListener listener){
		mClickListener = listener;
	}
	

	public interface OnActionClickListener {
		public void OnClickAction(int actionId);
	}

	public UpSlideMenuView(Context context) {
		super(context);
		init(context);
	}

	public UpSlideMenuView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context ctx) {
		mInflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		setGravity(Gravity.CENTER_VERTICAL);
		setOrientation(VERTICAL);
		LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		setLayoutParams(lp);

	}

	public void addAction(ArrayList<ActionItem> items) {
		if (mActions == null) {
			mActions = new ArrayList<ActionItem>();
		}

		mActions.addAll(items);
		invalidActions();
	}

	public void addAction(ActionItem item) {
		if (mActions == null) {
			mActions = new ArrayList<ActionItem>();
		}
		mActions.add(item);
		invalidActions();
	}

	private void invalidActions() {
		this.removeAllViews();
		if (mActions == null) {
			return;
		}
		for (ActionItem ai : mActions) {
			addView(createActionView(ai));
		}
	}

	private View createActionView(final ActionItem ai) {

		View view = mInflater.inflate(R.layout.item_menu_horizontal, null);
		TextView tv = (TextView) view.findViewById(R.id.tv);
		ImageView iv = (ImageView) view.findViewById(R.id.iv);
		
		if(!TextUtils.isEmpty(ai.getTitle())){
			tv.setText(ai.getTitle());
		}

		if(ai.getIcon() != 0){
			iv.setImageResource(ai.getIcon());
	
		}
	
		view.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				if (mClickListener != null) {
					mClickListener.OnClickAction(ai.getId());
				}
			}
		});
		return view;
	}
	
	public static  class ActionItem {
		
		public ActionItem(String title, int icon, int id) {
			this.title = title;
			this.iconId = icon;
			this.id = id;
		}

		private String title;
		private int iconId;
		private int id;
		
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public int getIcon() {
			return iconId;
		}
		public void setIcon(int icon) {
			this.iconId = icon;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
	}

}
